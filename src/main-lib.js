/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2022
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : main-lib.js
 * @Created_at  : 27/06/2022
 * @Update_at   : 27/06/2022
 * ----------------------------------------------------------------
 */

// Require common css
import "./assets/css/Variables.css";

// Require package elements

// --------------------------------
// Export components

export {
    // Components
};
