/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2022
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : PageConstant.vue
 * @Created_at  : 27/06/2022
 * @Update_at   : 27/06/2022
 * ----------------------------------------------------------------
 */

/**
 * Constants : Page.
 */
export default class PageConstant {
    // --------------------------------
    // Format

    static get FORMAT_A3() {
        return 'A3';
    }

    static get FORMAT_A4() {
        return 'A4';
    }

    static get FORMAT_A5() {
        return 'A5';
    }

    static get FORMAT_DEFAULT() {
        return this.FORMAT_A4;
    }
}
