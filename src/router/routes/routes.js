/**
 * ----------------------------------------------------------------
 * @Copyright   : SAS Spopit
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : routes.js
 * @Created_at  : 27/06/2022
 * @Update_at   : 27/06/2022
 * ----------------------------------------------------------------
 */

// Components
import CV from "../../components/Page/CV";

export default [
    // --------------------------------
    // Pages

    {
        path: '/',
        name: 'curriculum vitae',
        component: CV
    }
];
