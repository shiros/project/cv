/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2022
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : main.js
 * @Created_at  : 27/06/2022
 * @Update_at   : 27/06/2022
 * ----------------------------------------------------------------
 */

import Vue from "vue";

import App from "./App";
import Router from "./router/Router";

// --------------------------------
// Configs

Vue.config.productionTip = false;
Vue.config.performance = true;

// --------------------------------
// Constants

// Router
const router = Router.router;

// --------------------------------
// Register modules

// Router
Router.register();

// --------------------------------
// Entry point

let rootElement = '#app';
new Vue({
    router: router,
    render: h => h(App)
}).$mount(rootElement);
