/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2022
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : vue.config.js
 * @Created_at  : 27/06/2022
 * @Update_at   : 27/06/2022
 * ----------------------------------------------------------------
 */

module.exports = {
    css: {
        extract: false
    }
};
