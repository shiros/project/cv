# ShirOS Project - CV

This project is Alexandre Caillot curriculum vitae.

## Information
  
In this project, we'll use some external libs. You can see them below.

### Community

- [Vue](https://github.com/vuejs/vue), you can see the licence [here](https://github.com/vuejs/vue/blob/dev/LICENSE).
- [Vuex](https://github.com/vuejs/vuex), you can see the licence [here](https://github.com/vuejs/vuex/blob/master/LICENSE).

## Table of Contents

[[_TOC_]]

## Project

### NPM

```shell script
$ npm install
```

### Yarn

```shell script
$ yarn install
```

### Commands

- #### *Launch development server*

To launch the development server, use the project's command below.

```shell
$ npm run serve
```

- #### *Build package*

To build package for production environment or just deploy it, use the project's command below.

```shell
$ npm run build
```

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)
